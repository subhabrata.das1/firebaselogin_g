// ignore_for_file: public_member_api_docs, sort_constructors_first, unrelated_type_equality_checks
// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomTextField extends StatefulWidget {
  final bool inputformatter;

  final bool isSuffix;
  final bool isPrefix;
  final bool isOnuserInteraction;
  final String hintText;
  final String labelText;
  final bool isLabel;
  final TextInputType? textType;

  final String suffixIcon;
  final Icon? preffixicon;
  final TextEditingController controller;
  final bool isPassword;
  final bool readonly;
  final String? validatorType;

  const CustomTextField(
      {Key? key,
      this.isPrefix = false,
      this.preffixicon,
      this.isLabel = false,
      this.labelText = '',
      this.isSuffix = false,
      this.inputformatter = false,
      required this.hintText,
      this.suffixIcon = '',
      required this.controller,
      this.isPassword = false,
      this.textType,
      this.readonly = false,
      this.validatorType,
      // this.initial,
      this.isOnuserInteraction = false})
      : super(key: key);

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  //

  bool isObscure = false;
  void obscureOperation() {
    setState(() {
      isObscure = !isObscure;
    });
  }

  @override
  void initState() {
    if (widget.isPassword) {
      isObscure = true;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 35, vertical: 10),
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
        child: TextFormField(
          controller: widget.controller,
          style: TextStyle(fontSize: 20),
          keyboardType: widget.textType,
          obscureText: isObscure,
          readOnly: widget.readonly,
          autovalidateMode: widget.isOnuserInteraction
              ? AutovalidateMode.onUserInteraction
              : AutovalidateMode.disabled,
          inputFormatters: widget.inputformatter
              ? <TextInputFormatter>[
                  FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                  FilteringTextInputFormatter.digitsOnly
                ]
              : null,
          validator: (value) {
            if (widget.validatorType == "password") {
              if (RegExp(r"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)")
                  .hasMatch(value!)) {
                return null;
              }
              return "Please Enter a Valid Password";
            } else if (widget.validatorType == "email") {
              if (RegExp(
                      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                  .hasMatch(value!)) {
                return null;
              }
              return "Please Enter a Valid Email";
            } else {
              return null;
            }
          },
          decoration: InputDecoration(
              border: OutlineInputBorder(
                borderSide: BorderSide(
                  width: 0,
                  style: BorderStyle.none,
                ),
                borderRadius: BorderRadius.circular(20.0),
              ),
              filled: true,
              labelText: widget.isLabel ? widget.labelText : '',
              prefixIcon: widget.isPrefix ? widget.preffixicon : null,
              suffixIcon: widget.isSuffix
                  ? IconButton(
                      onPressed: () {
                        obscureOperation();
                      },
                      icon: Icon(
                        isObscure ? Icons.visibility : Icons.visibility_off,
                      ))
                  : null,
              hintStyle: GoogleFonts.lato(
                textStyle: TextStyle(color: Colors.grey, fontSize: 20),
              ),
              hintText: widget.hintText,
              fillColor: Colors.cyan[100]),
        ),
      ),
    );
  }
}
