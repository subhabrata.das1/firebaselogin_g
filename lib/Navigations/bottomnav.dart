// ignore_for_file: prefer_const_constructors

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebaselogin_g/views/addtask.dart';
import 'package:firebaselogin_g/views/homepage.dart';
import 'package:firebaselogin_g/views/register.dart';
import 'package:flutter/material.dart';

class BottomNav extends StatefulWidget {
  int? selectedIndex;

  BottomNav({this.selectedIndex, super.key});

  @override
  State<BottomNav> createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
  final List _widgetOptions = [
    HomePage(),
    AddTask(),
    Register(
      cd: FirebaseAuth.instance.currentUser!,
    ),
  ];

  void _onItemTapped(index) {
    setState(() {
      ind = index;
    });
  }

  User? user;
  late int ind;

  @override
  void initState() {
    super.initState();
    user = FirebaseAuth.instance.currentUser;
    if (widget.selectedIndex == null) {
      ind = 0;
    } else {
      ind = widget.selectedIndex!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: 'Home',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.add_task),
                label: 'Add Task',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                label: 'Register page',
              ),
            ],
            currentIndex: ind,
            selectedItemColor: Colors.redAccent,
            onTap: (index) {
              _onItemTapped(index);
            }),
        body: _widgetOptions[ind]);
  }
}
