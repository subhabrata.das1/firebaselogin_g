// ignore_for_file: prefer_const_constructors

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebaselogin_g/Navigations/bottomnav.dart';
import 'package:flutter/material.dart';

import '../widgets/textformfield.dart';

class EditCredential extends StatefulWidget {
  final DocumentSnapshot cd;
  const EditCredential({required this.cd, super.key});

  @override
  State<EditCredential> createState() => _EditCredentialState();
}

class _EditCredentialState extends State<EditCredential> {
  FirebaseAuth auth = FirebaseAuth.instance;

  TextEditingController phonetexteditingcontroller = TextEditingController();
  TextEditingController mailtexteditingcontroller = TextEditingController();
  TextEditingController nametexteditingcontroller = TextEditingController();

  CollectionReference users = FirebaseFirestore.instance.collection('users');

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      valueFromGmail();
      debugPrint(phonetexteditingcontroller.toString());
    });
  }

  editValue() async {
    await users.doc(widget.cd['uid']).update({
      'name': nametexteditingcontroller.text,
      'phone': int.parse(phonetexteditingcontroller.text),
    });
    if (!mounted) return;
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => BottomNav(
            selectedIndex: 1,
          ),
        ));
  }

  valueFromGmail() {
    nametexteditingcontroller.text = widget.cd['name'];
    mailtexteditingcontroller.text = widget.cd['email'];
    phonetexteditingcontroller.text = widget.cd['phone'].toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Edit Credential")),
      body: Center(
        child: SingleChildScrollView(
            child: Column(
          children: [
            CircleAvatar(
              radius: 50,
              backgroundImage: NetworkImage(
                widget.cd['imageurl'].toString(),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            CustomTextField(
              isLabel: true,

              labelText: 'Edit Name',
              // initial: inPass,
              isOnuserInteraction: true,
              hintText: "Name",
              isPrefix: true,
              preffixicon: Icon(Icons.person_outline_rounded),
              controller: nametexteditingcontroller,
            ),
            SizedBox(
              height: 20,
            ),
            //EmailTextField
            CustomTextField(
              isLabel: true,
              labelText: 'Email ID',
              // initial: inPass,
              isOnuserInteraction: true,
              hintText: "Email",
              readonly: true,
              isPrefix: true,
              preffixicon: Icon(Icons.mail_outline_rounded),
              controller: mailtexteditingcontroller,
            ),
            SizedBox(
              height: 20,
            ),
            //Phone Number Field
            CustomTextField(
              // initial: inPass,
              isLabel: true,
              inputformatter: true,
              labelText: 'Edit Phone Number',
              isOnuserInteraction: true,
              hintText: "Phone Number",
              isPrefix: true,
              preffixicon: Icon(Icons.call_outlined),
              controller: phonetexteditingcontroller,
            ),
            SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 20,
            ),
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.all(10),
                  backgroundColor: Colors.cyan, // Background color
                ),
                onPressed: () async {
                  editValue();
                },
                child: Text("Save")),
          ],
        )),
      ),
    );
  }
}
