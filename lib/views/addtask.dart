// ignore_for_file: prefer_const_constructors

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../loading/loading.dart';

class AddTask extends StatefulWidget {
  const AddTask({super.key});

  @override
  State<AddTask> createState() => _AddTaskState();
}

class _AddTaskState extends State<AddTask> {
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  FirebaseAuth auth = FirebaseAuth.instance;
  TextEditingController message = TextEditingController();
  final formKey = GlobalKey<FormState>();

  addValue() async {
    await users.doc(auth.currentUser!.uid).collection('tasks').add({
      'timestamp': DateTime.now().toString().substring(11, 19),
      'messege': message.text,
    }).whenComplete(() {
      Navigator.pop(context);
      message.text = '';
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        appBar: AppBar(
          title:
              Text(FirebaseAuth.instance.currentUser!.displayName!.toString()),
          leading: Container(
            padding: EdgeInsets.all(4),
            child: CircleAvatar(
                backgroundImage:
                    NetworkImage(FirebaseAuth.instance.currentUser!.photoURL!),
                radius: 10),
          ),
        ),
        backgroundColor: Color.fromARGB(151, 28, 66, 78),
        body: Container(
          padding: EdgeInsets.only(top: 20, right: 5, left: 5),
          alignment: Alignment.topCenter,
          child: SingleChildScrollView(
            child: Form(
              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(height: 150, child: Image.asset('assets/task.png')),
                  SizedBox(height: 20),
                  TextFormField(
                    // autovalidateMode: AutovalidateMode.onUserInteraction,
                    maxLines: 6,
                    controller: message,
                    decoration: InputDecoration(
                      hintText: 'Write some thing here...',
                      contentPadding: const EdgeInsets.only(
                          left: 14.0, bottom: 8.0, top: 8.0),
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                            color: Color.fromARGB(255, 225, 0, 0)),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Color.fromARGB(255, 225, 0, 0)),
                          borderRadius: BorderRadius.circular(10)),
                      errorStyle: const TextStyle(
                        color: Color.fromARGB(255, 255, 69, 69),
                        fontSize: 12.9,
                        fontFamily: 'Roboto-Italic',
                      ),
                    ),
                    validator: (value) {
                      return value!.isEmpty ? "empty message not allow" : null;
                    },
                  ),
                  SizedBox(height: 40),
                  ElevatedButton(
                      child: Text("Save"),
                      onPressed: () {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return LoadingPage("Loading...");
                            });
                        if (formKey.currentState!.validate()) {
                          addValue();
                        } else {
                          Navigator.pop(context);
                        }
                      })
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
