// ignore_for_file: prefer_const_constructors

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebaselogin_g/Navigations/bottomnav.dart';
import 'package:firebaselogin_g/views/loginpage.dart';
import 'package:flutter/material.dart';

import '../loading/loading.dart';
import '../widgets/textformfield.dart';

class Register extends StatefulWidget {
  final User cd;
  const Register({required this.cd, super.key});

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  TextEditingController phonetexteditingcontroller = TextEditingController();
  TextEditingController mailtexteditingcontroller = TextEditingController();
  TextEditingController nametexteditingcontroller = TextEditingController();

  FirebaseAuth auth = FirebaseAuth.instance;

  TextEditingController phonetexteditingcontroller1 = TextEditingController();
  TextEditingController mailtexteditingcontroller1 = TextEditingController();
  TextEditingController nametexteditingcontroller1 = TextEditingController();

  CollectionReference users = FirebaseFirestore.instance.collection('users');

  final Stream<DocumentSnapshot> _usersStream = FirebaseFirestore.instance
      .collection('users')
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .snapshots();

  logOut() {
    Navigator.pop(context);
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => LogIn()),
        (Route<dynamic> route) => false);
  }

  FirebaseFirestore fireStore = FirebaseFirestore.instance;

  User? user;
  bool exist = false;
  @override
  void initState() {
    super.initState();
    user = FirebaseAuth.instance.currentUser;

    WidgetsBinding.instance.addPostFrameCallback((_) {
      valueFromGmail();
    });
    checkExists();
  }

  checkExists() async {
    if (user != null) {
      await FirebaseFirestore.instance
          .collection('users')
          .doc(user!.uid)
          .get()
          .then((DocumentSnapshot documentSnapshot) {
        if (documentSnapshot.exists) {
          setState(() {
            exist = true;
          });
        } else {
          setState(() {
            exist = false;
          });
        }
      });
    }
  }

  Future addValue() async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return LoadingPage("");
        });
    await users.doc(auth.currentUser!.uid).set({
      'imageurl': auth.currentUser!.photoURL,
      'uid': auth.currentUser!.uid,
      'name': nametexteditingcontroller.text,
      'phone': int.parse(phonetexteditingcontroller.text),
      'email': auth.currentUser!.email,
    }).whenComplete(() => Navigator.pop(context));
  }

  valueFromGmail() {
    nametexteditingcontroller.text = auth.currentUser!.displayName.toString();
    mailtexteditingcontroller.text = auth.currentUser!.email.toString();
  }

  @override
  Widget build(BuildContext context) {
    return exist
        ? GestureDetector(
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: StreamBuilder<DocumentSnapshot>(
              stream: _usersStream,
              builder: (BuildContext context,
                  AsyncSnapshot<DocumentSnapshot> snapshot) {
                if (snapshot.hasError) {
                  return Text('Something went wrong');
                }

                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Text("Loading");
                }
                nametexteditingcontroller1.text =
                    snapshot.data!.get('name').toString();
                mailtexteditingcontroller1.text =
                    snapshot.data!.get('email').toString();
                phonetexteditingcontroller1.text =
                    snapshot.data!.get('phone').toString();

                return Scaffold(
                  appBar: AppBar(title: Text("Edit Profile")),
                  body: Center(
                    child: SingleChildScrollView(
                        child: Column(
                      children: [
                        CircleAvatar(
                          radius: 50,
                          backgroundImage: NetworkImage(
                            snapshot.data!.get('imageurl').toString(),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        CustomTextField(
                          isLabel: true,
                          labelText: 'Name',
                          isOnuserInteraction: true,
                          hintText: "Name",
                          isPrefix: true,
                          preffixicon: Icon(Icons.person_outline_rounded),
                          controller: nametexteditingcontroller1,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        //EmailTextField
                        CustomTextField(
                          isLabel: true,
                          labelText: 'Email ID',
                          readonly: true,
                          isOnuserInteraction: true,
                          hintText: "Email",
                          isPrefix: true,
                          preffixicon: Icon(Icons.mail_outline),
                          controller: mailtexteditingcontroller1,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        //Phone Number Field
                        CustomTextField(
                          isLabel: true,
                          labelText: 'Phone Number',
                          textType: TextInputType.phone,
                          isOnuserInteraction: true,
                          inputformatter: true,
                          hintText: "Phone Number",
                          isPrefix: true,
                          preffixicon: Icon(Icons.call_rounded),
                          controller: phonetexteditingcontroller1,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(10),
                              backgroundColor: Colors.cyan,
                            ),
                            onPressed: () async {
                              showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) {
                                    return LoadingPage("");
                                  });

                              await users
                                  .doc(
                                snapshot.data!.get('uid').toString(),
                              )
                                  .update({
                                'name': nametexteditingcontroller.text,
                                'phone':
                                    int.parse(phonetexteditingcontroller.text),
                              }).whenComplete(() => Navigator.pop(context));
                              if (!mounted) return;
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => BottomNav(),
                                  ));
                            },
                            child: Text("Save Changes")),
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(10),
                              backgroundColor: Colors.cyan,
                            ),
                            onPressed: () async {
                              showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) {
                                    return LoadingPage("");
                                  });
                              await FirebaseAuth.instance
                                  .signOut()
                                  .whenComplete(() => logOut());
                            },
                            child: Text("Log Out")),
                      ],
                    )),
                  ),
                );
              },
            ),
          )
        : Scaffold(
            appBar: AppBar(title: Text("Register")),
            body: Center(
              child: SingleChildScrollView(
                  child: Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  CustomTextField(
                    isLabel: true,
                    labelText: 'Name',
                    isOnuserInteraction: true,
                    hintText: "Name",
                    isPrefix: true,
                    preffixicon: Icon(Icons.person_outline_rounded),
                    controller: nametexteditingcontroller,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  //EmailTextField
                  CustomTextField(
                    isLabel: true,
                    labelText: 'Email ID',
                    // initial: inPass,
                    readonly: true,
                    isOnuserInteraction: true,
                    hintText: "Email",
                    isPrefix: true,
                    preffixicon: Icon(Icons.mail_outline_rounded),
                    controller: mailtexteditingcontroller,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  //Phone Number Field
                  CustomTextField(
                    isOnuserInteraction: true,
                    isLabel: true,
                    inputformatter: true,
                    labelText: 'Add Phone Number',
                    hintText: "Phone Number",
                    isPrefix: true,
                    preffixicon: Icon(Icons.call_outlined),
                    controller: phonetexteditingcontroller,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        padding: EdgeInsets.all(10),
                        backgroundColor: Colors.cyan,
                      ),
                      onPressed: () async {
                        addValue().whenComplete(() => Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => BottomNav(),
                            )));
                      },
                      child: Text("Register Your Details")),
                ],
              )),
            ),
          );
  }
}
