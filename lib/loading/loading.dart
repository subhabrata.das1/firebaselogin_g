import 'package:flutter/material.dart';

class LoadingPage extends StatelessWidget {
  String status = "";
  LoadingPage(this.status, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Center(
        child: Container(
          padding: const EdgeInsets.all(10),
          height: 300,
          width: 200,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(children: [
            Image.asset(
              "assets/gg.gif",
              height: 220,
              width: 200,
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              status,
              style: TextStyle(fontSize: 25, color: Colors.cyan.shade200
                  // color: Colors.black,
                  ),
            ),
          ]),
        ),
      ),
    );
  }
}
